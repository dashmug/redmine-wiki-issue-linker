"""
Instructions:

Modify the following constants according to the environment this script
is running in.

Then, rename this file from settings.sample.py to settings.py.
"""

REDMINE_SERVER = 'http://192.168.1.66'
KEY = ''

PROJECT_IDENTIFIER = 'ec-mps-base-certif'
PROJECT_ID = 36
WIKI_PAGE_TITLE = 'SRS'
HLR_TRACKER_ID = 5
SSS_REQ_TRACKER_ID = 15

WARNING_MESSAGE = 'p(conflict). WARNING: Do not update this issue subject and/or description. This issue was automatic created/updated by the following wiki page: {}/projects/{}/wiki/{}'.format(REDMINE_SERVER, PROJECT_IDENTIFIER, WIKI_PAGE_TITLE)
