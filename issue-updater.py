import re
from redmine import Redmine
from settings import (
    REDMINE_SERVER,
    KEY,
    PROJECT_IDENTIFIER,
    PROJECT_ID,
    WIKI_PAGE_TITLE,
    HLR_TRACKER_ID,
    SSS_REQ_TRACKER_ID,
    WARNING_MESSAGE
)

hlr_issues = None
hlr_issues_unused_ids = None
sss_req_issues = None
redmine = None

warnings = []
errors = []


class AnError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def connect_to_redmine():
    print 'Authenticating to Redmine server...'
    return Redmine(REDMINE_SERVER, key=KEY)


def get_HLR_issues():
    issues = redmine.issue.filter(project_id=PROJECT_ID,
                                  tracker_id=HLR_TRACKER_ID)

    hlr_issues = [issue for issue in issues if 'HLR' in issue.subject]

    return hlr_issues


def get_SSS_REQ_issues():
    issues = redmine.issue.filter(project_id=PROJECT_ID,
                                  tracker_id=SSS_REQ_TRACKER_ID)

    sss_req_issues = [issue for issue in issues if 'REQ-SSS' in issue.subject]

    return sss_req_issues


def get_tagged_issue_ids(tag):
        related_issue_ids = [issue.id for issue in sss_req_issues
                             if tag in issue.subject]

        return related_issue_ids


def remove_from_HLR_list(issue_identifier):
    global hlr_issues_unused_ids
    while issue_identifier in hlr_issues_unused_ids:
        hlr_issues_unused_ids.remove(issue_identifier)


def check_for_unused_HLRs():
    global warnings
    for unused_id in hlr_issues_unused_ids:
        warning_template = 'Unused HLR on ticket #{0}. See {1}/issues/{0}.'
        warning = warning_template.format(unused_id, REDMINE_SERVER)
        warnings.append(warning)


def display_warnings():
    if warnings != []:
        print ''
        print 'Warnings:'

        for warning in warnings:
            print '  {}'.format(warning)


def display_errors():
    if errors != []:
        print ''
        print 'Errors:'

        for error in errors:
            print '  {}'.format(error)


class HighLevelRequirementIssue:
    def __init__(self, tags):
        tags = tags.upper()
        tag_pattern = re.compile('<(HLR-(?:.)+?)>')
        self.identifier = tag_pattern.match(tags).group(1)
        tags = tags.replace(self.identifier, '').replace('<>', '')
        self.related_issues = []
        self.related_issue_ids = []
        self.get_related_issues(tags)
        self.description = ''
        self.id = self.get_issue_id()

    def get_issue_id(self):
        hlr_issue_ids = [issue.id for issue in hlr_issues
                         if self.identifier in issue.subject]
        if len(hlr_issue_ids) == 0:
            return None
        elif len(hlr_issue_ids) == 1:
            return hlr_issue_ids[0]
        else:
            raise AnError('We have duplicate issues with the same HLR tag')

    def get_related_issues(self, tags):
        sss_tag_pattern = re.compile('<<(REQ-SSS-[0-9]+)>>')
        match = sss_tag_pattern.match(tags)
        if match is not None:
            tag = match.group(1)
            if get_tagged_issue_ids(tag) == []:
                error_template = '{} is related to a non-existing "{}".'
                error = error_template.format(self.identifier, tag)
                print '  {}'.format(error)
                errors.append(error)
            else:
                if tag in self.related_issues:
                    error_template = '{} is already related to "{}".'
                    error = error_template.format(self.identifier, tag)
                    print '  {}'.format(error)
                    errors.append(error)
                else:
                    for id in get_tagged_issue_ids(tag):
                        self.related_issue_ids.append(id)
                    self.related_issues.append(tag)
            tags = re.sub('<<REQ-SSS-[0-9]+>>', '', tags, count=1)
            self.get_related_issues(tags)
        else:
            return

    @property
    def subject(self):
        """
        Shortens the description, adds the HLR tag and adds an ellipsis
        (...) to fit within the 255-character limit
        """
        char_limit = 255 - len(self.identifier) - 6
        if len(self.description) > char_limit:
            char_pos = char_limit
            description = self.description
            while description[char_pos-1:char_pos].isalnum() is True:
                char_pos -= 1

            description = self.description[:char_pos]
            template = '[{}] {}...'
        else:
            description = self.description
            template = '[{}] {}'

        return template.format(self.identifier, description.encode('utf-8'))

    def save(self):
        if self.id is None:
            redmine_issue = redmine.issue.new()
            print 'Added new HLR issue: {}...'.format(self.identifier)
        else:
            redmine_issue = redmine.issue.get(self.id)
            print 'Updated existing HLR issue: {}...'.format(self.identifier)
            remove_from_HLR_list(self.id)

        redmine_issue.project_id = PROJECT_IDENTIFIER
        redmine_issue.subject = self.subject

        self.description = self.description.encode('utf-8')
        redmine_issue.description = "{}\n\n{}".format(self.description,
                                                      WARNING_MESSAGE)
        redmine_issue.tracker_id = HLR_TRACKER_ID
        redmine_issue.save()

        if self.id is None:
            self.id = redmine_issue.id
        self.relations = redmine_issue.relations

    def __str__(self):
        return '{}: {}'.format(self.identifier, ', '.join(self.optional_tags))


def is_duplicate(issue, issues):
    count = sum(1 for each_issue in issues
                if each_issue.identifier == issue.identifier)
    return count > 1


def line_contains_tags(line_text):
    line_text = line_text.upper()
    tags_pattern = re.compile('<(HLR-(?:.)+)>')
    if tags_pattern.search(line_text) is not None:
        return True


def line_contains_headers(line_text):
    header_pattern = re.compile('(^h[1-4]{1})')
    if header_pattern.search(line_text) is not None:
        return True


def save_relation(issue_id, issue_to_id):
    try:
        redmine.issue_relation.create(issue_id=issue_id,
                                      issue_to_id=issue_to_id,
                                      relation_type='relates')
    except:
        pass


def main():
    global redmine
    global hlr_issues
    global hlr_issues_unused_ids
    global sss_req_issues
    redmine = connect_to_redmine()
    hlr_issues = get_HLR_issues()
    hlr_issues_unused_ids = [issue.id for issue in hlr_issues]
    sss_req_issues = get_SSS_REQ_issues()
    srs_page = redmine.wiki_page.get(WIKI_PAGE_TITLE,
                                     project_id=PROJECT_IDENTIFIER)
    lines = srs_page.text.split('\n')
    issues = []
    issue = None

    for line in lines:
        if line_contains_headers(line) and issue is not None:
            issues.append(issue)
            issue = None

        if line_contains_tags(line):
            if issue is not None:
                issues.append(issue)
            issue = HighLevelRequirementIssue(line)
        elif line != '' and issue is not None:
            issue.description = ' '.join([issue.description, line]).strip()
    else:
        if issue is not None:
            issues.append(issue)

    for issue in issues:
        if is_duplicate(issue, issues):
            error = 'Duplicate HLR Issue "{}".'.format(issue.identifier)
            print 'Error: {}'.format(error)
            if error not in errors:
                errors.append(error)
        else:
            issue.save()

            for related_issue in issue.relations:
                redmine.issue_relation.delete(related_issue.id)

            for index, id in enumerate(issue.related_issue_ids):
                save_relation(issue.id, id)
                related_issue_tag = issue.related_issues[index]
                print '  Saved related issue: {}'.format(related_issue_tag)

if __name__ == "__main__":
    main()
    check_for_unused_HLRs()
    display_warnings()
    display_errors()
